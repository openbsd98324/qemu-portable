
# QEMU Portable for Win


## Links


https://gitlab.com/openbsd98324/qemu-portable/-/raw/main/archive/qemu-w64-setup-20220418.exe

````
qemu-system-x86_64 -cpu pentium -bios /usr/share/seabios/bios.bin -enable-kvm -m 128 -cdrom ~/Downloads/"Windows2000 .iso" -drive file=Windows2000.qcow2 -netdev user,id=n0 -device rtl8139,netdev=n0 -vga cirrus

````

## Medias

Running under qemu portable live: 

![](medias/1715346183-qemu-ubuntu.png)


